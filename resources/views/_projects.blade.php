<div class="projects mb-3">
    <div class="card shadow project mb-4">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-sm-5 project-thumbnail-container p-0">
                    <img src="{{ asset('images/projects/condochicks.png') }}"
                         alt="project_name"
                         class="project-thumbnail">
                </div>

                <div class="col-sm-7 pl-4 pr-5 py-3">
                    <h2 class="h5 border-bottom pb-2">
                        <a href="http://condochicks.com"
                           target="_blank"
                           class="btn-link h-100"
                           title="http://condochicks.com">Condochicks</a>
                    </h2>
                    <p>
                        @php
                            $projectDesc = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto dicta
                                            doloribus eum fugit porro. Ab assumenda eius eligendi ex excepturi fugit
                                            impedit iste quam rerum sint suscipit ut, velit, veritatis.';

                        @endphp

                        {{ substr($projectDesc, 0, 255).'...' }}
                    </p>
                    <p>
                        <span class="project-skill wordpress-logo mr-2"></span>
                        <span class="project-skill bootstrap-logo mr-2"></span>
                    </p>
                    <a href="#"
                       class="trigger"
                       data-izimodal-open="#projectModal"
                       data-izimodal-transitionin="fadeInDown">Read more</a>
                </div>
            </div>
        </div>
    </div><!-- end project -->

    <div class="card shadow project mb-4">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-sm-5 project-thumbnail-container p-0">
                    <img src="{{ asset('images/projects/apartbookings.png') }}"
                         alt="project_name"
                         class="project-thumbnail">
                </div>

                <div class="col-sm-7 pl-4 pr-5 py-3">
                    <h2 class="h5 border-bottom pb-2">
                        <a href="http://apartbookings.aparts.dev.standby.team"
                           target="_blank"
                           class="btn-link h-100"
                           title="http://apartbookings.com">Apartbookings</a>
                    </h2>
                    <p>
                        @php
                            $projectDesc = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto dicta
                                            doloribus eum fugit porro. Ab assumenda eius eligendi ex excepturi fugit
                                            impedit iste quam rerum sint suscipit ut, velit, veritatis.';
                        @endphp

                        {{  substr($projectDesc, 0, 255).'...' }}
                    </p>
                    <p>
                        <span class="project-skill laravel-logo mr-2"></span>
                        <span class="project-skill vuejs-logo mr-2"></span>
                        <span class="project-skill sass-logo mr-2"></span>
                        <span class="project-skill stripe-logo mr-2"></span>
                        <span class="project-skill docker-logo mr-2"></span>
                    </p>
                    <a href="#">Read more</a>
                </div>
            </div>
        </div>
    </div><!-- end project -->


    <div class="card shadow project mb-4">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-sm-5 project-thumbnail-container p-0">
                    <img src="{{ asset('images/projects/hoian-now.png') }}"
                         alt="project_name"
                         class="project-thumbnail">
                </div>

                <div class="col-sm-7 pl-4 pr-5 py-3">
                    <h2 class="h5 border-bottom pb-2">
                        <a href="https://hoiannow.com/"
                           target="_blank"
                           class="btn-link h-100"
                           title="http://hoiannow.com">Hoian Now</a>
                    </h2>
                    <p>
                        @php
                            $projectDesc = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto dicta
                                            doloribus eum fugit porro. Ab assumenda eius eligendi ex excepturi fugit
                                            impedit iste quam rerum sint suscipit ut, velit, veritatis.';
                        @endphp

                        {{  substr($projectDesc, 0, 255).'...' }}
                    </p>
                    <p>
                        <span class="project-skill wordpress-logo mr-2"></span>
                    </p>
                    <a href="#">Read more</a>
                </div>
            </div>
        </div>
    </div><!-- end project -->


    <div class="card shadow project mb-4">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-sm-5 project-thumbnail-container p-0">
                    <img src="{{ asset('images/projects/top-fotooboi.png') }}"
                         alt="project_name"
                         class="project-thumbnail">
                </div>

                <div class="col-sm-7 pl-4 pr-5 py-3">
                    <h2 class="h5 border-bottom pb-2">
                        <a href="https://topfotooboi.ru"
                           target="_blank"
                           class="btn-link h-100"
                           title="https://top-fotooboi.ru">Топ фотообои</a>
                    </h2>
                    <p>
                        @php
                            $projectDesc = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto dicta
                                            doloribus eum fugit porro. Ab assumenda eius eligendi ex excepturi fugit
                                            impedit iste quam rerum sint suscipit ut, velit, veritatis.';
                        @endphp

                        {{  substr($projectDesc, 0, 255).'...' }}
                    </p>
                    <p>
                        <span class="project-skill wordpress-logo mr-2"></span>
                        <span class="project-skill woocommerce-logo mr-2"></span>
                    </p>
                    <a href="#">Read more</a>
                </div>
            </div>
        </div>
    </div><!-- end project -->

    <div class="card shadow project mb-4">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-sm-5 project-thumbnail-container p-0">
                    <img src="{{ asset('images/projects/phoenixmi.png') }}"
                         alt="project_name"
                         class="project-thumbnail">
                </div>

                <div class="col-sm-7 pl-4 pr-5 py-3">
                    <h2 class="h5 border-bottom pb-2">
                        <a href="https://phoenixmi.com"
                           target="_blank"
                           class="btn-link h-100"
                           title="https://www.phoenixmi.com">Phoenix MI</a>
                    </h2>
                    <p>
                        @php
                            $projectDesc = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto dicta
                                            doloribus eum fugit porro. Ab assumenda eius eligendi ex excepturi fugit
                                            impedit iste quam rerum sint suscipit ut, velit, veritatis.';
                        @endphp

                        {{  substr($projectDesc, 0, 255).'...' }}
                    </p>
                    <p>
                        <span class="project-skill wordpress-logo mr-2"></span>
                        <span class="project-skill bem-logo mr-2"></span>
                        <span class="project-skill sass-logo mr-2"></span>
                    </p>
                    <a href="#">Read more</a>
                </div>
            </div>
        </div>
    </div><!-- end project -->

</div>

<nav aria-label="Page navigation">
    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">Next</a></li>
    </ul>
</nav>

@include('_project_view_modal')

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js"></script>

    <script>
        $('#projectModal').iziModal({
            width: '960px',
            top: $('main').offset().top,
            overlayColor: 'rgba(0, 0, 0, 0.6)',
        });
    </script>
@endsection