<div id="projectModal">
    <div class="project-modal p-3">
        <div class="row project-modal-content">
            <div class="col">
                <img src="/images/projects/phoenixmi.png" alt="Phoenix image" class="w-100 shadow-lg">
            </div>

            <div class="col">
                <div class="project-title">
                    <h2 class="mb-3">
                        <span class="text-secondary">Phoenix</span>

                        <span class="float-right">
                            <span class="pl-2">
                                <span class="project-skill wordpress-logo mr-2"></span>
                                <span class="project-skill bem-logo mr-2"></span>
                                <span class="project-skill sass-logo mr-2"></span>
                            </span>
                        </span>
                    </h2>
                </div>

                <div class="project-description mt-4 text-justify">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias, aspernatur dolores ipsam libero modi
                        natus officia possimus reiciendis sed? Ab aut deserunt eius hic modi molestiae omnis possimus voluptate.</p>
                    <p>Architecto aspernatur aut autem blanditiis commodi dignissimos ducimus eaque earum et ex explicabo fugiat itaque
                        libero modi molestias mollitia nulla qui quidem recusandae reprehenderit repudiandae tenetur ullam ut, velit
                        veniam.</p>
                    <p>Ab architecto error eveniet expedita illo ipsa, ipsam nemo nulla, optio sed sequi sint? Ad animi aspernatur
                        blanditiis ea ex fuga natus odit porro quisquam soluta, tempore tenetur ullam voluptatem!</p>
                </div>

                <div class="project-attributes">
                    <p class="text-black-50 m-0">
                        <i class="fas fa-link text-secondary"></i>
                        <a href="https://www.phoenixmi.com" class="btn btn-link pl-2" target="_blank">https://www.phoenixmi.com</a>
                    </p>
                    <p class="text-black-50 m-0">
                        <i class="fas fa-calendar-alt text-orange"></i>
                        <span class="pl-2">June, 2017 - Jul, 2017</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>