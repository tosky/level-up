@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif

<div class="card">
    <div class="card-header bg-transparent">
        <i class="fas fa-id-card text-primary"></i>
        <span class="text-muted pl-1 pt-1">John Doe</span>
    </div>

    <div class="card-body">
        <form>
            <div class="row">
                <div class="col-3">
                    {{-- Avatar --}}
                    <div class="form-group text-center">
                        <div>
                            <img src="{{ asset('images/avatars/avatar.svg') }}" class="border rounded-circle" alt="avatar" width="140">
                        </div>

                    </div>
                    <div class="form-group mx-auto mt-4 collapse">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="avatar">
                            <label for="avatar" class="custom-file-label lup-custom-file-label text-center text-black-">Choose avatar</label>
                        </div>
                    </div>
                </div>

                <div class="col">
                    {{-- Name --}}
                    <div class="form-group row">
                        <label for="firstName" class="col-sm-2 col-form-label">Name:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" aria-label="First name" id="firstName" class="form-control" placeholder="John">
                                <input type="text" aria-label="Last name" id="lastName" class="form-control" placeholder="Doe">
                            </div>
                        </div>
                    </div>

                    {{-- Email --}}
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="john@doe.com">
                        </div>
                    </div>

                    {{-- Birthday --}}
                    <div class="form-group row">
                        <label for="dateOfBirth" class="col-sm-2 col-form-label">Birthday:</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-4">
                                    <select name="day" id="day" class="custom-select">
                                        @foreach(range(1, 31) as $number)
                                            <option value="{{ $number }}" @if($number == 14) selected @endif>{{ $number }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="month" id="month" class="custom-select">
                                        @php
                                            $months = [
                                            'January', 'February', 'March', 'April', 'May', 'June', 'July',
                                            'August', 'September', 'October', 'November', 'December'
                                            ];
                                        @endphp
                                        @foreach($months as $number)
                                            <option value="{{ $number }}" @if($number == 'May') selected @endif>{{ $number }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="year" id="year" class="custom-select">
                                        @foreach(range(1900, 2000) as $number)
                                            <option value="{{ $number }}" @if($number == 1987) selected @endif>{{ $number }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="dateOfBirth" value="14-5-1987">
                    </div>
                </div>
            </div>

        </form>
    </div>
</div><!-- end card main-->

{{-- About me card --}}
<div class="card mt-4">
    <div class="card-header bg-transparent pr-1">
        <i class="fas fa-book text-success"></i>
        <span class="text-muted pl-1 pt-1">About me</span>

        <div class="float-right">
            <button class="btn btn-link btn-sm text-primary pt-0" data-toggle="tooltip" title="Edit">
                <i class="fas fa-pen"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-sm-12">
                <div class="about-content">
                    <p>Hello,</p>

                    <p>I have analytical, innovative and inquisitive famous Russian mind, strong math background. I am
                        goal-oriented, sociable, capable to learn, open-minded; responsibility and active life position.</p>

                    <p>Expert in developing unique high-load Python as well as PHP-projects based on Laravel 4/5. Works
                        exclusively with Laravel for the past few years.</p>

                    <p>I work in the web since 2013  and done tons of tasks.</p>

                    <p>My skills are:</p>
                    <p>Python/Django, PHP, Yii, Laravel, Wordpress, JavaScript, HTML5, jQuery, AJAX, oAuth, XML, Soap, SVN,
                        Git, Agile, Adaptive and Responsive layouts.</p>

                    <p>I'm ready to learn, and learn quickly... any framework that needs to be used in your task.</p>

                    <p>I will be glad to work with you!</p>
                </div>
            </div>
        </div>
    </div>
</div><!-- end about me card -->

{{-- Experience card --}}
<div class="card mt-4">
    <div class="card-header bg-transparent">
        <i class="fas fa-brain text-pink"></i>
        <span class="text-muted pl-1 pt-1">Experience</span>
    </div>
    <div class="card-body">
        {{--Level--}}
        <div class="form-group row">
            <label for="level" class="col-sm-2">Level: </label>
            <div class="col-sm-10">
                <input type="range" class="custom-range disabled" id="level" min="1" max="3" step="1" value="2" disabled>
                <small id="levelHelp" class="form-text text-muted">Middle</small>
            </div>
        </div>

        {{--Skills--}}
        <div class="form-group row">
            <label for="skills" class="col-sm-2">Skills: </label>
            <div class="col-sm-10">
                {{--<select multiple="multiple" name="skills[]" id="skills" class="custom-select add-select2">--}}
                {{--<option value="52">CSS</option>--}}
                {{--<option value="93">HTML</option>--}}
                {{--<option value="22">PHP</option>--}}
                {{--<option value="4">Git</option>--}}
                {{--<option value="34">MySql</option>--}}
                {{--<option value="77">Python</option>--}}
                {{--</select>--}}

                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">CSS3</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">HTML5</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">PHP</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">Git</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">MySql</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">Python</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">Django</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">jQuery</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">Bootstrap</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">Apache</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">nginx</small>
                <small class="border rounded p-1 text-black-50 border-muted d-inline-block mb-1">Docker</small>
            </div>
        </div>
    </div>
</div><!-- end experience card -->

{{-- Location card --}}
<div class="card mt-4">
    <div class="card-header bg-transparent">
        <i class="fas fa-map-marker text-info"></i>
        <span class="text-muted pl-1 pt-1">Location</span>
    </div>
    <div class="card-body">
        {{--Location--}}
        <div class="form-group row">
            <label for="country" class="col-sm-2 col-form-label">Location: </label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-6">
                        <select class="custom-select" id="country">
                            <option selected>United Kingdom</option>
                            <option value="29">Russia</option>
                            <option value="273">USA</option>
                            <option value="844">France</option>
                        </select>
                    </div>

                    <div class="col-sm-6">
                        <select class="custom-select" id="city">
                            <option selected>London</option>
                            <option value="634">Moscow</option>
                            <option value="4588">Omsk</option>
                            <option value="327">St. Petersburg</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        {{-- Address --}}
        <div class="form-group row">
            <label for="address" class="col-sm-2 col-form-label">Address: </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="address" placeholder="221b Baker Street">
            </div>
        </div>

        {{-- Phone --}}
        <div class="form-group row">
            <label for="phone" class="col-sm-2 col-form-label">Phone: </label>
            <div class="col-sm-10">
                <input type="tel" class="form-control" id="phone" placeholder="+7 234 567 8910">
            </div>
        </div>
    </div>
</div><!-- end card location -->

{{-- Social card --}}
<div class="card mt-4">
    <div class="card-header bg-transparent">
        <i class="fas fa-paper-plane text-orange"></i>
        <span class="text-muted pl-1 pt-1">Social accounts</span>
    </div>
    <div class="card-body">
        {{-- Socials --}}
        <div class="form-group row">
            <div class="col">
                <a href="#" class="btn btn-outline-secondary border w-100">
                    <i class="fab fa-google"></i>
                    Google
                </a>
            </div>

            <div class="col">
                <a href="#" class="btn btn-outline-secondary border w-100">
                    <i class="fab fa-vk"></i>
                    VK
                </a>
            </div>

            <div class="col">
                <a href="#" class="btn btn-outline-secondary border w-100">
                    <i class="fab fa-github"></i>
                    Github
                </a>
            </div>
        </div>
    </div>
</div><!-- end social card -->


{{-- Security card --}}
<div class="card mt-4">
    <div class="card-header bg-transparent">
        <i class="fas fa-unlock-alt text-secondary"></i>
        <span class="text-muted pl-1 pt-1">Security</span>
    </div>
    <div class="card-body">
        {{-- Password --}}
        <div class="form-group row">
            <label for="password" class="col-sm-2 pr-0 col-form-label">Password:</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <input type="password"
                           class="form-control bg-transparent"
                           placeholder="******"
                           data-toggle="modal"
                           data-target="#changePassword"
                           readonly>
                </div>
            </div>
        </div>
    </div>
</div><!-- end security card -->

@section('scripts')
    @parent
@endsection