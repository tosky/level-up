@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.css" />
@endsection

@section('content')
<div class="container">
    @include('_change_password_modal')

    <div class="row">
        <div class="col-md-3">
            <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action py-2 active"
                   id="listProfileList"
                   data-toggle="list"
                   href="#list-profile"
                   role="tab"
                   aria-controls="profile">Profile</a>

                <a class="list-group-item list-group-item-action py-2"
                   id="listProjectsList"
                   data-toggle="list"
                   href="#list-projects"
                   role="tab"
                   aria-controls="projects">Projects</a>

                {{--<a class="list-group-item list-group-item-action py-2"--}}
                   {{--id="listExercisesList"--}}
                   {{--data-toggle="list"--}}
                   {{--href="#list-exercises"--}}
                   {{--role="tab"--}}
                   {{--aria-controls="projects">Exercises</a>--}}
            </div>
        </div>

        <div class="col-md-9 content">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                    @include('_profile')
                </div>
                <div class="tab-pane fade" id="list-projects" role="tabpanel" aria-labelledby="list-projects-list">
                    @include('_projects')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        // Change level range section
        let levels = {
            1: 'Junior',
            2: 'Middle',
            3: 'Senior'
        };

        let levelsClasses = {
            1: 'text-md-left',
            2: 'text-md-center',
            3: 'text-md-right',
        };

        let level = $("#level");
        let currentLevelClass = levelsClasses[level.val()];

        $("#levelHelp").addClass(currentLevelClass);

        level.change(function(){
            let index = $(this).val();
            let levelHelp = $("#levelHelp");

            levelHelp.removeClass(currentLevelClass);

            currentLevelClass = levelsClasses[index];
            levelHelp.addClass(currentLevelClass).text(levels[index]);
        });

        // Select2
        $('.add-select2').select2();
        $('#skills').select2({
            placeholder: "Select skills"
        });

        // Tooltip
        $('[data-toggle="tooltip"]').tooltip();
    </script>
@endsection